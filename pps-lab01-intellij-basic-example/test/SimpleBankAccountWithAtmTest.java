import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccount;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the SimpleBankAccountWithAtm implementation
 */
class SimpleBankAccountWithAtmTest extends SimpleBankAccountTest {

    private static final String NAME = "Mario";
    private static final String SURNAME = "Rossi";
    private static final int ID = 1;
    private static final int INIT_BALANCE = 0;

    @Override
    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder(NAME, SURNAME, ID);
        bankAccount = new SimpleBankAccountWithAtm(accountHolder, INIT_BALANCE);
    }

    @Override
    @Test
    void testDeposit() {
        super.testDeposit();
        setAtm(true);
        bankAccount.deposit(accountHolder.getId(), 100);
        assertEquals(199, bankAccount.getBalance());
    }

    @Override
    @Test
    void testWrongDeposit() {
        super.testWrongDeposit();
        setAtm(true);
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.deposit(2, 50);
        assertEquals(199, bankAccount.getBalance());
    }

    @Override
    @Test
    void testWithdraw() {
        super.testWithdraw();
        setAtm(true);
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 70);
        assertEquals(58, bankAccount.getBalance());
    }

    @Override
    @Test
    void testWrongWithdraw() {
        super.testWrongWithdraw();
        setAtm(true);
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(2, 70);
        assertEquals(199, bankAccount.getBalance());
    }

    private void setAtm(final boolean useAtm) {
        ((SimpleBankAccountWithAtm)bankAccount).useAtm(useAtm);
    }
}
