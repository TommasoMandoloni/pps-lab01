package lab01.example.model;

/**
 * Allowing deposit and withdraw with ATM
 */
public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccount {

    private boolean useAtm;
    private static final int FEE = 1; // ATM fee

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance){
        super(holder, balance);
        useAtm = false;
    }

    @Override
    public void deposit(final int usrID, final double amount) {
        double cash = useAtm ? amount-FEE : amount;
        super.deposit(usrID, cash);
    }

    @Override
    public void withdraw(final int usrID, final double amount) {
        double cash = useAtm ? amount+FEE : amount;
        super.withdraw(usrID, cash);
    }

    public void useAtm(final boolean use) {
        this.useAtm = use;
    }

}
