import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList circularList;
    private static final int SIZE = 5;
    private SelectStrategy strategy;
    private static final boolean EVEN = true;
    private static final boolean ODD = false;
    private static final int MULTIPLE = 5;
    private static final int NUMBER = 3;
    private final StrategyFactory strategyFactory = new StrategyFactoryImpl();

    @BeforeEach
    void setup(){
        this.circularList = new SimpleCircularList();
        this.testInitiallyEmpty();
    }

    private void testInitiallyEmpty(){
        assertTrue(circularList.isEmpty());
    }

    @Test
    void testAdd(){
        circularList.add(0);
        assertFalse(circularList.isEmpty());
        assertEquals(1, circularList.size());
    }

    @Test
    void testSize(){
        assertEquals(0, circularList.size());
        fillList();
        assertFalse(circularList.isEmpty());
        assertEquals(SIZE, circularList.size());
    }

    @Test
    void testNext(){
        assertEquals(Optional.empty(), circularList.next());
        fillList();
        IntStream.range(0, SIZE).forEach(e -> assertEquals(Optional.of(e), circularList.next()));
        assertEquals(Optional.of(0), circularList.next()); // Test circularity of the list
    }

    @Test
    void testPrevious(){
        assertEquals(Optional.empty(), circularList.previous());
        fillList();
        assertEquals(Optional.of(0), circularList.next());
        assertEquals(Optional.of(1), circularList.next());
        assertEquals(Optional.of(0), circularList.previous());
        assertEquals(Optional.of(4), circularList.previous()); // Test circularity of the list
    }

    @Test
    void testCircularity(){
        fillList(); // {0, 1, 2, 3, 4}
        IntStream.range(0, SIZE).forEach(e -> circularList.next());
        assertEquals(Optional.of(0), circularList.next());
        circularList.reset();
        IntStream.range(0, 2).forEach(e -> circularList.next());
        circularList.previous();
        assertEquals(Optional.of(4), circularList.previous());
    }

    @Test
    void testReset(){
        fillList();
        IntStream.range(0, 3).forEach(e -> circularList.next());
        assertNotEquals(Optional.of(0), circularList.next());
        circularList.reset();
        assertEquals(Optional.of(0), circularList.next());
    }

    @Test
    void testOddEvenStrategy(){
        testOddStrategy();
        resetList();
        testEvenStrategy();
    }

    private void resetList(){
        circularList.reset();
        circularList = new SimpleCircularList();
    }

    private void testOddStrategy(){
        selectStrategy(strategyFactory.createOddEvenStrategy(ODD));
        circularList.add(0);
        circularList.add(2);
        circularList.add(4);
        circularList.add(6);
        circularList.add(8);
        // test if the strategy fails but the index still retrieve the right next element
        assertEquals(Optional.of(0), circularList.next());
        assertEquals(Optional.empty(), circularList.next(strategy));
        assertEquals(Optional.of(2), circularList.next());
        circularList.add(3);
        circularList.add(10);
        assertEquals(Optional.of(3), circularList.next(strategy));
        assertEquals(Optional.of(3), circularList.next(strategy)); // test right circularity because of single element
        IntStream.range(0, 3).forEach(e -> circularList.next());
        assertEquals(Optional.of(3), circularList.next(strategy)); // find element from not initial index
    }

    private void testEvenStrategy(){
        selectStrategy(strategyFactory.createOddEvenStrategy(EVEN));
        circularList.add(1);
        circularList.add(3);
        circularList.add(5);
        circularList.add(7);
        circularList.add(9);
        // test if the strategy fails but the index still retrieve the right next element
        assertEquals(Optional.of(1), circularList.next());
        assertEquals(Optional.empty(), circularList.next(strategy));
        assertEquals(Optional.of(3), circularList.next());
        circularList.add(2);
        circularList.add(11);
        assertEquals(Optional.of(2), circularList.next(strategy));
        assertEquals(Optional.of(2), circularList.next(strategy)); // test right circularity because of single element
        IntStream.range(0, 3).forEach(e -> circularList.next());
        assertEquals(Optional.of(2), circularList.next(strategy)); // find element from not initial index
    }

    @Test
    void testMultipleOfStrategy(){
        selectStrategy(strategyFactory.createMultipleOfStrategy(MULTIPLE));
        circularList.add(1);
        circularList.add(3);
        circularList.add(7);
        circularList.add(9);
        // test if the strategy fails but the index still retrieve the right next element
        assertEquals(Optional.of(1), circularList.next());
        assertEquals(Optional.empty(), circularList.next(strategy));
        assertEquals(Optional.of(3), circularList.next());
        circularList.add(5);
        assertEquals(Optional.of(5), circularList.next(strategy));
        assertEquals(Optional.of(5), circularList.next(strategy)); // test right circularity because of single element
        IntStream.range(0, 2).forEach(e -> circularList.next());
        assertEquals(Optional.of(7), circularList.next());
        assertEquals(Optional.of(5), circularList.next(strategy)); // find element from not initial index
    }

    @Test
    void testEqualsStrategy(){
        selectStrategy(strategyFactory.createEqualsStrategy(NUMBER));
        circularList.add(0);
        circularList.add(1);
        circularList.add(2);
        circularList.add(4);
        circularList.add(5);
        // test if the strategy fails but the index still retrieve the right next element
        assertEquals(Optional.of(0), circularList.next());
        assertEquals(Optional.empty(), circularList.next(strategy));
        assertEquals(Optional.of(1), circularList.next());
        circularList.add(3);
        circularList.add(5);
        assertEquals(Optional.of(3), circularList.next(strategy));
        assertEquals(Optional.of(3), circularList.next(strategy)); // test right circularity because of single element
        IntStream.range(0, 2).forEach(e -> circularList.next());
        assertEquals(Optional.of(1), circularList.next());
        assertEquals(Optional.of(3), circularList.next(strategy)); // find element from not initial index
    }

    /**
     * Fill the list with @size elements from 0 to @size-1
     */
    private void fillList() {
        for(int i = 0; i < SIZE; i++){
            circularList.add(i);
        }
    }

    /**
     * Select strategy to retrieve the next element of the list
     * @param strategy the strategy to apply
     */
    private void selectStrategy(SelectStrategy strategy){
        this.strategy = strategy;
    }
}
