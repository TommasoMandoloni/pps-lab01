package lab01.tdd;

public class StrategyFactoryImpl implements StrategyFactory{
    @Override
    public SelectStrategy createOddEvenStrategy(boolean even) {
        return new OddEvenStrategy(even);
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int multiple) {
        return new MultipleOfStrategy(multiple);
    }

    @Override
    public SelectStrategy createEqualsStrategy(int number) {
        return new EqualsStrategy(number);
    }
}
