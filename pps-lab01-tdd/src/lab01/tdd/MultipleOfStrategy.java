package lab01.tdd;

public class MultipleOfStrategy implements SelectStrategy{

    private final int multiple;

    public MultipleOfStrategy(final int multiple){
        this.multiple = multiple;
    }

    @Override
    public boolean apply(int element) {
        return element % multiple == 0;
    }
}
