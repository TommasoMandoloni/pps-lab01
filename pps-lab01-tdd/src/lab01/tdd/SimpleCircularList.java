package lab01.tdd;

import java.util.*;

public class SimpleCircularList implements CircularList{

    private final List<Integer> list;
    private int index;

    public SimpleCircularList(){
        this.list = new ArrayList<>();
        this.index = 0;
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if(isEmpty()) return Optional.empty();
        nextIndex();
        return Optional.of(list.get(this.index++));
    }

    @Override
    public Optional<Integer> previous() {
        if(isEmpty()) return Optional.empty();
        prevIndex();
        return Optional.of(list.get(this.index++));
    }

    @Override
    public void reset() {
        this.index = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if(isEmpty()) return Optional.empty();
        // retrieve the next element selected first occurrence
        for(int i = 0; i < size(); i++){ // check all the element of the list in case of absent occurrence (don't care about order, but number of iterations counts)
            Optional<Integer> element = next(); // index handling inside 'next' method
            if(strategy.apply(element.get())) return element; // don't need to check if present because of first control on empty list
        }
        return Optional.empty(); // not found elements with property specified by strategy
    }

    private void nextIndex() {
        this.index = index == this.size() ? 0 : index;
    }

    private void prevIndex() {
        this.index = index-2 < 0 ? this.size()-1 : index-2;
    }
}
