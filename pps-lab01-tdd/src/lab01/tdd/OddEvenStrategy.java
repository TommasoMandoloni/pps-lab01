package lab01.tdd;

public class OddEvenStrategy implements SelectStrategy{

    private final boolean even;

    public OddEvenStrategy(final boolean even){
        this.even = even;
    }

    @Override
    public boolean apply(int element) {
        return this.even == (element % 2 == 0);
    }
}
