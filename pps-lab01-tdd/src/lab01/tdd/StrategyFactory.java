package lab01.tdd;

public interface StrategyFactory {

    SelectStrategy createOddEvenStrategy(boolean even);

    SelectStrategy createMultipleOfStrategy(int multiple);

    SelectStrategy createEqualsStrategy(int number);
}
